# Alphabet-Frequency-Counter
Program that counts the number of occurrences of each letter in the alphabet in a given paragraph.

### Prerequisites

Java Development Kit (http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Text Editor (What I use Visual Studio Code - https://code.visualstudio.com/)

# Example
![alt text](https://media.giphy.com/media/3o6fJcTYz0v4QbGee4/giphy.gif)

## Author/s

* **Sean Ong** - [ASean___](https://gitlab.com/ASean___)